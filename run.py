from pathlib import Path

from species_predictor_api.business_logic.prediction_model.model_creator import ModelCreator
from species_predictor_api.business_logic.prediction_model.species_predictor_model import SpeciesPredictorModel
from species_predictor_api.config import MODEL_PICKLE_FILE_PATH
from species_predictor_api.species_predictor_api import SpeciesPredictorApi


def main() -> None:
    if not Path(MODEL_PICKLE_FILE_PATH).exists():
        ModelCreator.create_model_pickle(MODEL_PICKLE_FILE_PATH)

    SpeciesPredictorModel.load_model(MODEL_PICKLE_FILE_PATH)

    species_predictor_api = SpeciesPredictorApi()
    species_predictor_api.run()


if __name__ == '__main__':
    main()
