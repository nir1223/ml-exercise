import os
from dotenv import load_dotenv

load_dotenv()

HOST = os.getenv('HOST')
PORT = os.getenv('PORT')
MODEL_PICKLE_FILE_PATH = os.getenv('MODEL_PICKLE_FILE_PATH')

FEATURES_NAMES = ['sepal length', 'sepal width', 'petal length', 'petal width']
MODEL_TEST_SIZE = 0.3
RANDOM_FOREST_ESTIMATORS = 100
