from flask import Flask

from species_predictor_api.blueprints.error_handler import error_handler
from species_predictor_api.blueprints.model_prediction_routes import model_prediction_routes
from species_predictor_api.config import HOST, PORT


class SpeciesPredictorApi:
    def __init__(self):
        self._app = Flask(__name__)
        self._app.register_blueprint(model_prediction_routes)
        self._app.register_blueprint(error_handler)

    def run(self, host=HOST, port=PORT):
        self._app.run(host=host, port=port)
