import pickle

from numpy import ndarray
from pandas import DataFrame


class SpeciesPredictorModel:
    _model = None

    @classmethod
    def load_model(cls, file_path: str):
        cls._model = pickle.load(open(file_path, 'rb'))

    @classmethod
    def predict_species(cls, features: DataFrame) -> ndarray:
        predictions: ndarray = cls._model.predict(features)
        return predictions
