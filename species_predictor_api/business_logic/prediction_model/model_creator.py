from sklearn import datasets
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import pickle

from species_predictor_api.config import MODEL_TEST_SIZE, RANDOM_FOREST_ESTIMATORS


class ModelCreator:
    @classmethod
    def create_model_pickle(cls, file_path: str):
        iris_dataset = datasets.load_iris()
        iris_features = pd.DataFrame({
            'sepal length': iris_dataset.data[:, 0],
            'sepal width': iris_dataset.data[:, 1],
            'petal length': iris_dataset.data[:, 2],
            'petal width': iris_dataset.data[:, 3]
        })

        target_names = iris_dataset.target_names
        target = [target_names[target_index] for target_index in iris_dataset.target]

        x_train, x_test, y_train, y_test = train_test_split(iris_features, target, test_size=MODEL_TEST_SIZE)

        random_forest_classifier = RandomForestClassifier(n_estimators=RANDOM_FOREST_ESTIMATORS)
        random_forest_classifier.fit(x_train, y_train)

        pickle.dump(random_forest_classifier, open(file_path, 'wb'))
