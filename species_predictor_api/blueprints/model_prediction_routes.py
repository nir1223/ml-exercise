from typing import Dict

from flask import Blueprint, jsonify
from flask_restful.reqparse import RequestParser
import pandas as pd
from numpy import ndarray

from species_predictor_api.blueprints.request_parser.predict_request_parser import PredictRequestParser
from species_predictor_api.business_logic.prediction_model.species_predictor_model import SpeciesPredictorModel

model_prediction_routes = Blueprint('model_prediction', __name__)


@model_prediction_routes.route('/predict', methods=['POST'])
def predict():
    predict_request_parser: RequestParser = PredictRequestParser.create_predict_request_parser()
    parsed_predict_args: Dict[str, float] = predict_request_parser.parse_args(strict=True)

    features = pd.DataFrame([parsed_predict_args])
    species_predictions_array: ndarray = SpeciesPredictorModel.predict_species(features)
    species_prediction: str = species_predictions_array.tolist()[0]

    return jsonify(species_prediction), 201
