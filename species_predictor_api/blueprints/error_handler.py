from typing import Tuple

from flask import Blueprint, Response
from werkzeug.exceptions import HTTPException
from flask import jsonify

error_handler = Blueprint('error_handler', __name__)


@error_handler.app_errorhandler(400)
def bad_request(http_exception: HTTPException) -> Tuple[Response, int]:
    # Formats the exceptions thrown by RequestParser
    response_body = http_exception.data if hasattr(http_exception, 'data') else http_exception.description
    return jsonify(response_body), 400
