from flask_restful.reqparse import RequestParser

from species_predictor_api.config import FEATURES_NAMES


class PredictRequestParser:
    @classmethod
    def create_predict_request_parser(cls) -> RequestParser:
        feature_names = FEATURES_NAMES
        predict_request_parser = RequestParser()
        for feature_name in feature_names:
            predict_request_parser.add_argument(
                feature_name,
                type=float,
                help=f'{feature_name} is required',
                required=True,
                nullable=False
            )

        return predict_request_parser
